// Setup expre dependency
const express = require("express");
// Create a Router instance
const router = express.Router();
// Import TaskController.js
const taskController = require("../controllers/taskController");

// Routes

// Route to get all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})

// Route to create POST a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for deleting taask
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// ACTIVITY
// Route to get all tasks
router.get("/:id", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})

// Route to create POST a new task
router.post("/:id", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for updating a task
router.put("/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

/* Activity
1. Create a route for getting a specific task.

2. Create a controller function for retrieving a specific task.

3. Return the result back to the client/Postman(Routes).

4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

5. Create a route for changing the status of a task to "complete".

6. Create a controller function for changing the status of a task to "complete".

7. Return the result back to the client/Postman(Routes).

8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

9. Export the Modules collection and add it to s31 folder.

*/

module.exports = router;
